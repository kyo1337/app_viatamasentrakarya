<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = 'home/error_page404';
$route['translate_uri_dashes'] = FALSE;

/* ------------------------ Route About ------------------------ */
$route['about-us'] = 'about/index';
$route['syarat-dan-ketentuan'] = 'about/syaratdanketentuan';
$route['galery'] = 'about/galery';
/* ------------------------------------------------------------- */

/* ------------------------ Route Layanan ------------------------ */
$route['layanan'] = 'layanan/index';
$route['layanan/pendirian-perusahaan'] = 'layanan/pendirian_perusahaan';
$route['layanan/sertifikasi-badan-usaha'] = 'layanan/sertifikasi_badan_usaha';
$route['layanan/virtual-office'] = 'layanan/virtual_office';
$route['layanan/space-office'] = 'layanan/space_office';
$route['layanan/outsourcing-security'] = 'layanan/outsourcing_security';
$route['layanan/layanan-tambahan'] = 'layanan/layanan_tambahan';
/* ------------------------------------------------------------- */

/* ------------------------ Route Informasi ------------------------ */
$route['informasi/kbli-terbaru'] = 'informasi/kbli_terbaru';
$route['informasi/download-kbli-terbaru'] = 'informasi/download_kbli_terbaru';
$route['informasi/cek-zonasi'] = 'informasi/cek_zonasi';
$route['informasi/download-table-zonasi'] = 'informasi/download_table_zonasi';
$route['informasi/karir'] = 'informasi/karir';
$route['informasi/contact-us'] = 'informasi/contact_us';
/* ------------------------------------------------------------- */