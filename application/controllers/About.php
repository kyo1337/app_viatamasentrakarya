<?php
defined('BASEPATH') or exit('No direct script access allowed');

class About extends CI_Controller
{
    public function index()
    {
        $data = [
            'pages' => 'About Us'
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('about/about-us', $data);
        $this->load->view('templates/footer');
    }

    public function syaratdanketentuan()
    {
        $data = [
            'pages' => 'Syarat Dan Ketentuan'
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('about/syaratdanketentuan', $data);
        $this->load->view('templates/footer');
    }

    public function galery()
    {
        $dir = 'assets/img/galery/';
        $image = glob($dir . '*.webp');

        $data = [
            'pages' => 'Galery',
            'images' => $image
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('about/galery', $data);
        $this->load->view('templates/footer');
    }
}
