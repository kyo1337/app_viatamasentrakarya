<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function index()
    {
        $dir = 'assets/img/clients/';
        $image = glob($dir . '*.webp');

        $data = [
            'images' => $image
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('index', $data);
        $this->load->view('templates/footer');
    }

    public function syaratdanketentuan()
    {
        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('syaratdanketentuan');
        $this->load->view('templates/footer');
    }

    public function error_page404()
    {
        $this->load->view('errors/404_error.php');
    }
}
