<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Informasi extends CI_Controller
{
    public function kbli_terbaru()
    {
        $data = [
            'pages' => 'KBLI Terbaru'
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('informasi/kbli-terbaru', $data);
        $this->load->view('templates/footer');
    }

    public function download_kbli_terbaru()
    {
        $dir = "assets/berkas/";
        $filename = "kbli-2020-new.pdf";
        $file_path = $dir . $filename;
        $ctype = "application/octet-stream";
        if (!empty($file_path) && file_exists($file_path)) { /*check keberadaan file*/
            header("Pragma:public");
            header("Expired:0");
            header("Cache-Control:must-revalidate");
            header("Content-Control:public");
            header("Content-Description: File Transfer");
            header("Content-Type: $ctype");
            header("Content-Disposition:attachment; filename=\"" . basename($file_path) . "\"");
            header("Content-Transfer-Encoding:binary");
            header("Content-Length:" . filesize($file_path));
            flush();
            readfile($file_path);
            exit();
        } else {
            echo "The File does not exist.";
        }
    }

    public function cek_zonasi()
    {
        $data = [
            'pages' => 'Cek Zonasi'
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('informasi/cek-zonasi', $data);
        $this->load->view('templates/footer');
    }

    public function download_table_zonasi()
    {
        $dir = "assets/berkas/";
        $filename = "Tabel_Zonasi.pdf";
        $file_path = $dir . $filename;
        $ctype = "application/octet-stream";
        if (!empty($file_path) && file_exists($file_path)) { /*check keberadaan file*/
            header("Pragma:public");
            header("Expired:0");
            header("Cache-Control:must-revalidate");
            header("Content-Control:public");
            header("Content-Description: File Transfer");
            header("Content-Type: $ctype");
            header("Content-Disposition:attachment; filename=\"" . basename($file_path) . "\"");
            header("Content-Transfer-Encoding:binary");
            header("Content-Length:" . filesize($file_path));
            flush();
            readfile($file_path);
            exit();
        } else {
            echo "The File does not exist.";
        }
    }

    public function karir_coming_soon()
    {
        $data = [
            'pages' => 'Karir'
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('informasi/karir', $data);
        $this->load->view('templates/footer');
    }

    public function contact_us()
    {
        $data = [
            'pages' => 'Contact US'
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('informasi/contact-us', $data);
        $this->load->view('templates/footer');
    }
}
