<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Layanan extends CI_Controller
{
    public function index()
    {
        $data = [
            'pages' => 'Layanan'
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('layanan/list-layanan', $data);
        $this->load->view('templates/footer');
    }

    public function pendirian_perusahaan()
    {
        $data = [
            'pages' => 'Layanan',
            'pages_breadcrumbs' => 'Pendirian Perusahaan Resmi Kemenkumham RI',
            'number' => '6282116169922'
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('layanan/pendirian-perusahaan', $data);
        $this->load->view('templates/footer');
    }

    public function virtual_office()
    {
        $data = [
            'pages' => 'Layanan',
            'pages_breadcrumbs' => 'Virtual Office',
            'number' => '6282116169922'
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('layanan/virtual-office', $data);
        $this->load->view('templates/footer');
    }

    public function space_office()
    {
        $data = [
            'pages' => 'Layanan',
            'pages_breadcrumbs' => 'Space Office',
            'number' => '6282116169922'
        ];

        $this->load->view('templates/head');
        $this->load->view('templates/header');
        $this->load->view('layanan/space-office', $data);
        $this->load->view('templates/footer');
    }
}
