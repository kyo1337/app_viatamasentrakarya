<main id="main">
	<!-- ======= Breadcrumbs ======= -->
	<div class="breadcrumbs">
		<div class="container">
			<div class="d-flex justify-content-between align-items-center">
				<h2><?= $pages ?></h2>
				<ol>
					<li><a href="<?= base_url() ?>">Home</a></li>
					<li><?= $pages ?></li>
				</ol>
			</div>
		</div>
	</div>
	<!-- End Breadcrumbs -->

	<section class="inner-page">
		<div class="container" data-aos="fade-up">
			<section id="portfolio" class="portfolio" data-aos="fade-up">
				<div
					class="portfolio-isotope"
					data-portfolio-filter="*"
					data-portfolio-layout="masonry"
					data-portfolio-sort="original-order"
				>
					<div class="row g-2">
						<?php foreach ($images as $img) : ?>
						<div
							class="col-xl-4 col-lg-4 col-md-6 portfolio-item d-flex align-content-center"
						>
							<img src="<?= $img ?>" class="img-fluid" alt="" />
							<div class="portfolio-info">
								<a
									href="<?= $img ?>"
									data-gallery="portfolio-gallery"
									class="glightbox preview-link"
									><i class="bi bi-zoom-in"></i
								></a>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</section>
		</div>
	</section>
</main>
