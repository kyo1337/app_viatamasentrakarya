<main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2><?= $pages ?></h2>
                <ol>
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li>
                        <a href="<?= base_url('layanan') ?>"><?= $pages ?></a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <section id="featured-services" class="featured-services">
        <div class="container">
            <div class="section-header">
                <h2>PILIH LAYANAN KEBUTUHAN ANDA</h2>
            </div>

            <div class="row gy-1">
                <div class="col-xl-4 col-md-6 col-sm-1" data-aos="zoom-out">
                    <div class="service-item">
                        <div class="icon text-center">
                            <i class="bi bi-activity icon"></i>
                        </div>
                        <h4 class="text-center">
                            <a href="<?= base_url('layanan/pendirian-perusahaan') ?>" class="stretched-link">Pendirian Perusahaan</a>
                        </h4>
                        <ul>
                            <li><i class="bx bx-check"></i> PT Perorangan</li>
                            <li><i class="bx bx-check"></i> PT</li>
                            <li><i class="bx bx-check"></i>CV</li>
                            <li><i class="bx bx-check"></i>PMA</li>
                            <li><i class="bx bx-check"></i>Yayasan</li>
                            <li><i class="bx bx-check"></i>Firma</li>
                        </ul>
                    </div>
                </div>

                <div class="col-xl-4 col-md-6 col-sm-1" data-aos="zoom-out" data-aos-delay="200">
                    <div class="service-item">
                        <div class="icon text-center">
                            <i class="bi bi-bounding-box-circles icon"></i>
                        </div>
                        <h4 class="text-center">
                            <a href="<?= base_url('layanan/sertifikasi-badan-usaha') ?>" class="stretched-link">Setifikasi Badan Usaha</a>
                        </h4>
                        <ul>
                            <li><i class="bx bx-check"></i> SBUJK</li>
                            <li><i class="bx bx-check"></i> SBUJPTL</li>
                            <li><i class="bx bx-check"></i> SMK3 Kemnaker</li>
                        </ul>
                    </div>
                </div>

                <div class="col-xl-4 col-md-6 col-sm-1" data-aos="zoom-out" data-aos-delay="400">
                    <div class="service-item">
                        <div class="icon text-center">
                            <i class="bi bi-calendar4-week icon"></i>
                        </div>
                        <h4 class="text-center">
                            <a href="<?= base_url('layanan/layanan-tambahan') ?>" class="stretched-link">Layanan Lainnya</a>
                        </h4>
                        <ul>
                            <li><i class="bx bx-check"></i> Perubahaan Akta</li>
                            <li>
                                <i class="bx bx-check"></i> Izin OSS (NIB & Sertifikat Standar)
                            </li>
                            <li><i class="bx bx-check"></i> Visa & KITAS</li>
                            <li><i class="bx bx-check"></i> Pendaftaran HAKI</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>