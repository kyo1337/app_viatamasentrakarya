<!-- ======= Footer ======= -->
<footer id="footer" class="footer">
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-info">
                        <div class="text-center">
                            <img src="<?= base_url() ?>assets/img/logo-portrait-2.png" alt="Logo Viatama Sentrakarya" width="250px" height="300px" />
                        </div>
                        <div class="mt-4">
                            <strong>Email Utama:</strong> admin@viatama.co.id
                            <br />
                            <strong>Phone:</strong> 021 2853-7846
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 footer-newsletter">
                    <h4>Navigation</h4>
                    <ul class="fs-6">
                        <li class="mb-3"><i class="bx bx-chevron-right"></i><a href="<?= base_url('layanan') ?>" class="text-white">Layanan</a></li>
                        <li class="mb-3"><i class="bx bx-chevron-right"></i><a href="<?= base_url('galery') ?>" class="text-white">Galery</a></li>
                        <li class="mb-3"><i class="bx bx-chevron-right"></i><a href="<?= base_url() ?>" class="text-white">Kantor</a></li>
                        <li class="mb-3">
                            <i class="bx bx-chevron-right"></i><a href="<?= base_url() ?>" class="text-white">Perusahaan</a>
                        </li>
                        <li class="mb-3">
                            <i class="bx bx-chevron-right"></i><a href="<?= base_url() ?>" class="text-white">Artikel</a>
                        </li>
                        <li class="mb-3"><i class="bx bx-chevron-right"></i><a href="<?= base_url('informasi/contact-us') ?>" class="text-white">Kontak</a></li>
                        <li class="mb-3">
                            <i class="bx bx-chevron-right"></i><a href="<?= base_url() ?>" class="text-white">Kebijakan</a>
                        </li>
                        <li class="mb-3">
                            <i class="bx bx-chevron-right"></i><a href="<?= base_url('syarat-dan-ketentuan') ?>" class="text-white">Syarat & Ketentuan</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-newsletter">
                    <h4>DKI Jakarta</h4>
                    <p class="fs-6 text-capitalize">
                        JL. Manunggal Pratama No.8 RT011/RW06 Cipinang Melayu. Makasar
                        Jakarta Timur 13620
                    </p>
                    <div class="mb-3">
                        <strong>Email Utama:</strong> admin@viatama.co.id
                        <strong>Phone:</strong> 021 2853-7846
                    </div>
                    <div class="mb-3">
                        <strong>Email VO/SO:</strong> cs@viatama.co.id
                        <strong>Whatsapp:</strong> 0821-1616-9922
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 footer-newsletter">
                    <h4>Tangerang</h4>
                    <p class="fs-6 text-capitalize">
                        Kawasan Industri Benua Permai Lestari, Jl. Raya Serang Desa No.Km. 25, 6, Cisereh, Kec. Tigaraksa, Kabupaten Tangerang, Banten 15720
                    </p>
                    <h4>Bekasi</h4>
                    <p class="fs-6 text-capitalize">
                        Kawasan industri Jababeka 1, jln Jababeka raya blok V no 59.
                        Cikarang, Bekasi 10220
                    </p>
                    <h4>Jepara</h4>
                    <p class="fs-6 text-capitalize">
                        Jln kudus jepara km 21, Desa sengonbugel Rt 01 Rw 01, Kec. Mayong kabupaten jepara 59465
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-legal text-center">
        <div class="container">
            <div class="social-links order-first order-lg-last mb-3 mb-lg-0">
                <!-- <a href="#" class="twitter"><i class="bi bi-twitter"></i></a> -->
                <!-- <a href="#" class="facebook"><i class="bi bi-facebook"></i></a> -->
                <a href="https://www.instagram.com/viatama.co.id/" class="instagram"><i class="bi bi-instagram"></i></a>
                <!-- <a href="#" class="google-plus"><i class="bi bi-skype"></i></a> -->
                <!-- <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a> -->
            </div>

            <div class="copyright">
                &copy; Copyright <strong><span>PT Viatama Sentrakarya</span></strong>. All Rights Reserved
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->

<!-- Vendor JS Files -->
<script src="<?= base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/aos/aos.js"></script>
<script src="<?= base_url() ?>assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="<?= base_url() ?>assets/js/main.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/vendor/whatsapp/js/floating-wpp.min.js"></script>
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/whatsapp/css/floating-wpp.min.css" />

<div id="myDiv"></div>
<script type="text/javascript">
    $(function() {
        $("#myDiv").floatingWhatsApp({
            phone: "6282116169922",
            popupMessage: "Hello, how can we help you?",
            showPopup: true,
            position: "right",
            size: "60px",
        });
    });
</script>
</body>

</html>