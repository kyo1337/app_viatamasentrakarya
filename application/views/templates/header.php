<body>
    <!-- ======= Header ======= -->
    <header id="header" class="header fixed-top bg-white">
        <div class="container d-flex align-items-center justify-content-between">
            <a href="<?= base_url() ?>" class="logo d-flex align-items-center scrollto me-auto me-lg-0">
                <!-- Uncomment the line below if you also wish to use an image logo -->
                <img src="<?= base_url() ?>assets/img/logo.png" alt="Logo Viatama Sentrakarya" />
            </a>

            <nav id=" navbar" class="navbar">
                <ul>
                    <li>
                        <a class="nav-link scrollto" href="<?= base_url() ?>">Home</a>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <span>About</span><i class="bi bi-chevron-down dropdown-indicator"></i>
                        </a>
                        <ul>
                            <li><a href="<?= base_url() ?>about-us">About Us</a></li>
                            <li><a href="<?= base_url() ?>galery">Galery</a></li>
                            <li>
                                <a href="<?= base_url() ?>syarat-dan-ketentuan">Syarat & Ketentuan</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <span>Layanan</span><i class="bi bi-chevron-down dropdown-indicator"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="<?= base_url() ?>layanan/pendirian-perusahaan">Pendirian Perusahaan</a>
                            </li>
                            <li><a href="<?= base_url() ?>layanan/sertifikasi-badan-usaha">Sertifikasi Badan Usaha</a></li>
                            <li>
                                <a href="<?= base_url() ?>layanan/virtual-office">Virtual Office</a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>layanan/space-office">Space Office</a>
                            </li>
                            <li><a href="<?= base_url() ?>layanan/outsourcing-security">Outsourcing Security</a></li>
                            <li><a href="<?= base_url() ?>layanan/layanan-tambahan">Layanan Tambahan</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <span>Informasi</span><i class="bi bi-chevron-down dropdown-indicator"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="<?= base_url() ?>informasi/kbli-terbaru">KBLI Terbaru</a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>informasi/cek-zonasi">Cek Zonasi</a>
                            </li>
                            <li><a href="<?= base_url() ?>informasi/karir">Karir</a></li>
                            <li>
                                <a href="<?= base_url() ?>informasi/contact-us">Hubungi Kami</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle d-none mt-3"></i>
            </nav>
        </div>
    </header>
    <!-- End Header -->